import client from "api/client"
import routes from "api/routes"

class TestService {
    getTestInfo = async (testID: number) => {
        const route = `${routes.tests}/${testID}`
        return await client.get(route)
    }

    createLogTest = async (testID: number) => {
        const bodys = new URLSearchParams()
        bodys.append("test_id", testID.toString())

        const route = `${routes.tests}/${testID}/logs`
        return await client.post(route, bodys)
    }

    getListTestTitle = async () => {
        const route = `${routes.tests}`
        return await client.get(route)
    }

}

const testService = new TestService()

export default testService

