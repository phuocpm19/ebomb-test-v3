import "./App.scss"
import React from "react"
import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import UserAccount from "components/screens/profile/UserAccountScreen"
import FullTestScreen from "components/screens/test/FullTestScreen"
import TestOnlineScreen from "components/screens/test/TestOnlineScreen"
import TestingScreen from "components/screens/test/TestingScreen"

const App = () => {
  return (
    <Router>
      <Switch>
        <Route path="/testing">
          <TestingScreen />
        </Route>
        <Route path="/user-account">
          <UserAccount />
        </Route>
        <Route path="/fulltest">
          <FullTestScreen />
        </Route>
        <Route path="/">
          <TestOnlineScreen />
        </Route>
      </Switch>
    </Router>
  )
}

export default App
