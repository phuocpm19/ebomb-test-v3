import React from "react"
import { Modal, Button } from "react-bootstrap"
import { Link } from "react-router-dom"

type ResultViewModalProps = {
    isShowModal: boolean
    Closed: any
}

const ResultViewModal = (props: ResultViewModalProps) => {
    return (
        <Modal show={props.isShowModal} onHide={props.Closed} className="modal-submit">
            <Modal.Header closeButton>
                <Modal.Title>
                    <button type="button" className="close"></button>
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div>
                    <div className="title">Chúc mừng bạn đã hoàn thành bài thi</div>
                    <div className="sub-title">Interview Skills Test 1</div>
                    <p className="text">Điểm của bạn</p>
                    <div className="circle">
                        <span className="point">500/990</span>
                    </div>
                    <p className="text">Đáp án đúng: 0 / 30</p>
                    <p className="text">Số câu hoàn thành: 0 / 30 câu</p>
                    <div className="box-button">
                        {/* <a className="btn btn-blue-ebomb">Nhận tư vấn lộ trình học</a> */}
                        {/* <a className="btn btn-blue-ebomb">Làm lại bài test</a> */}
                        <Link to="/fulltest" className="btn btn-blue-ebomb">Làm lại bài test</Link>
                        {/* <a className="btn btn-blue-ebomb">Xem đáp án</a> */}
                    </div>
                </div>
            </Modal.Body>

            <Modal.Footer>
                <Button variant="secondary" onClick={props.Closed}>
                    Close
                </Button>
            </Modal.Footer>
        </Modal>
    )
}

export default ResultViewModal