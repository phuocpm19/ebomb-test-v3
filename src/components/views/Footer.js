import React, { Component } from 'react'

export default class Footer extends Component {
    render() {
        return (
            <footer className="footer active-pc">
                <p className="text">© 2019 - Một sản phẩm của Công ty cổ phần giáo dục trực tuyến Biển học</p>
                <p className="text">VP Hà Nội: Tầng 5, Tòa Nhà 35 Tô Vĩnh Diện, Quận Thanh Xuân, TP Hà Nội</p>
            </footer>
        )
    }
}
