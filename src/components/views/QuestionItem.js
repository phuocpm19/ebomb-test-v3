import React, { Component } from 'react'

export default class QuestionItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            statusChoice: this.props.statusChoice
        }
    }
    onHandleChoice = () => {
        this.setState({
            statusChoice: !this.state.statusChoice
        })
    }
    render() {
        const { itemChoice, iTemContent } = this.props;
        const { statusChoice } = this.state;
        return (
            <li>
                <div className="item">
                    <div className={statusChoice === false ? 'item__choice' : 'item__choice item-choiced'} onClick={this.onHandleChoice}>{itemChoice}</div>
                    <div className="item__content" onClick={this.onHandleChoice}>{iTemContent}</div>
                </div>
            </li>
        )
    }
}
