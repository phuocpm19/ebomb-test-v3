import "bootstrap/dist/css/bootstrap.min.css"
import React from "react"
import { Button, Tabs, Tab } from "react-bootstrap"
import { Link } from "react-router-dom"
import Header from "components/views/Header"
import SecHead from "components/views/SecHead"
import QuestionItem from "components/views/QuestionItem"
import TabListQuestion from "components/views/TabListQuestion"
import ListPart from "components/views/ListPart"
import HeaderMobile from "components/views/HeaderMobile"
import { MAROON5 } from "utils/Audios"
import ResultViewModal from "components/views/test/ResultViewModal"

type AnswerModel = {
    id: number
    alpha: string
    title: string
    isSelect: boolean
    status: boolean
}

type QuestionModel = {
    partType: number
    id: number
    title?: string
    answer: AnswerModel[]
}

const TestingScreen = () => {
    const [isShowModal, setIsShowModal] = React.useState(false)
    const [keyTabs, setKeyTabs] = React.useState("home")
    const [statusListQuaestion, setStatusListQuaestion] = React.useState(false)
    const [questions, setQuestions] = React.useState<QuestionModel[]>([])
    const [questionIndex, setQuestionIndex] = React.useState(0)

    const closeModal = () => {
        setIsShowModal(false)
    }

    const openModal = () => {
        setIsShowModal(true)
    }

    const onHandleToggle = () => {
        setStatusListQuaestion(!statusListQuaestion)
    }

    let elmQuestionPartOne = null
    if (questions.length > 0) {
        elmQuestionPartOne = questions[questionIndex].answer.map((item: AnswerModel, index: number) => {
            return <QuestionItem
                key={item.id}
                itemChoice={item.alpha}
                statusChoice={item.status}
            />
        })
    }

    return (
        <div className="test-page">
            <Header />
            <HeaderMobile />
            <section className="main-content bg-f3">
                <div className="test-detail test-detail-pc active-pc">
                    <div className="container-fluid">
                        <div className="main-content__child">
                            <div className="row">
                                <div className="col-lg-2 aside aside-left" />
                                <div className="col-lg-8 test-content-common test-content-common-1">
                                    <div className="test-part-common">
                                        <SecHead heading="Part I: Picture Description"
                                            subHeading="Look at the picture and listen to the sentences. Choose
                                                the sentence that best describes the picture:"/>
                                        <div className="sec-body sec-body-common-2">
                                            <div className="box-picture">
                                                <div className="image img-scaledown">
                                                    <img src={""} alt="img question" />
                                                </div>
                                            </div>
                                            <div className="box-audio">
                                                <audio controls>
                                                    <source src={MAROON5} type="audio/ogg" />
                                                    <source src={MAROON5} type="audio/mpeg" />
                                                            Your browser does not support the audio element.
                                                    </audio>
                                            </div>
                                            <div className="box-question">
                                                <div className="box-question__title">
                                                    Question 1:
                                                    </div>
                                                <div className="box-question__content">
                                                    <ul className="list-question">
                                                        {elmQuestionPartOne}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-lg-2 aside aside-right" />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="test-detail test-detail-mb active-mb">
                    <div className="container">
                        <div className="test-detail-mb-content">
                            <div className="r-tab-custom">
                                <Tabs id="controlled-tab-example"
                                    activeKey={keyTabs}
                                    onSelect={keyTabs => setKeyTabs(keyTabs!)}
                                    className="r-tab-list">
                                    <Tab eventKey="home" title="Bài Test" className="r-tab-item">
                                        <div className="r-tab-content tab-1">
                                            <div className="test-part-common">
                                                <SecHead heading="Part I: Picture Description"
                                                    subHeading="Look at the picture and listen to the sentences. Choose
                                                        the sentence that best describes the picture:"/>
                                                <div className="sec-body sec-body-common-2">
                                                    <div className="box-picture">
                                                        <div className="image img-scaledown">
                                                            <img src={""} alt="img question" />
                                                        </div>
                                                    </div>
                                                    <div className="box-audio">
                                                        <audio controls>
                                                            <source src={MAROON5} type="audio/ogg" />
                                                            <source src={MAROON5} type="audio/mpeg" />
                                                            Your browser does not support the audio element.
                                                    </audio>
                                                    </div>
                                                    <div className="box-question">
                                                        <div className="box-question__title">
                                                            Question 1:
                                                        </div>
                                                        <div className="box-question__content">
                                                            <ul className="list-question">
                                                                {elmQuestionPartOne}
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </Tab>
                                    <Tab eventKey="profile" title="Bảng câu hỏi" className="r-tab-item">
                                        <div className="r-tab-content tab-2">
                                            <TabListQuestion />
                                        </div>
                                    </Tab>
                                    <Tab eventKey="contact" title="Xem các Part" className="r-tab-item">
                                        <div className="r-tab-content tab-3">
                                            <ListPart />
                                        </div>
                                    </Tab>
                                </Tabs>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section id="boxTime" className="box-time box-time-pc active-pc">
                <div className="container-fluid">
                    <div className="row row-mg-0">
                        <div className="col-lg-2 col-pd-0" />
                        <div className="col-lg-8 col-pd-0">
                            <div className="box-time-content">
                                <div className="box-time-content__left">
                                    <div className="box-button">
                                        <button id="buttonClickShowTableQuestion" type="button" className="btn btn-primary ml0" onClick={onHandleToggle}>Bảng
                                                câu hỏi</button>
                                    </div>
                                </div>
                                <div className="box-time-content__center">
                                    <i className="fa fa-clock-o" aria-hidden="true" />
                                    <span id="ten-countdown" />
                                </div>
                                <div className="box-time-content__right">
                                    <div className="box-button">
                                        <a className="btn btn-cyan">Next Question<i className="fa fa-chevron-circle-right" aria-hidden="true" /></a>
                                        <Link to="/part-2" className="btn btn-cyan">Next Part<i className="fa fa-arrow-circle-right" aria-hidden="true" /></Link>
                                        <Button className="btn btn-default" onClick={openModal}>
                                            Nộp bài
                                            </Button>
                                    </div>
                                </div>
                                <div id="tableQuestion" className={statusListQuaestion === false ? "table-question dp-none" : "table-question dp-block"} >
                                    <div className="title">Bảng câu hỏi</div>
                                    <div className="content">
                                        <ul className="list-number">
                                            <li><span className="checked">1</span></li>
                                            <li><span>2</span></li>
                                            <li><span>3</span></li>
                                            <li><span>4</span></li>
                                            <li><span>5</span></li>
                                            <li><span>6</span></li>
                                            <li><span>7</span></li>
                                            <li><span>8</span></li>
                                            <li><span>9</span></li>
                                            <li><span>10</span></li>
                                            <li><span>11</span></li>
                                            <li><span>12</span></li>
                                            <li><span>13</span></li>
                                            <li><span>14</span></li>
                                            <li><span>15</span></li>
                                            <li><span>16</span></li>
                                            <li><span>17</span></li>
                                            <li><span>18</span></li>
                                            <li><span>19</span></li>
                                            <li><span>20</span></li>
                                            <li><span>21</span></li>
                                            <li><span>22</span></li>
                                            <li><span>23</span></li>
                                            <li><span>24</span></li>
                                            <li><span>25</span></li>
                                            <li><span>26</span></li>
                                            <li><span>27</span></li>
                                            <li><span>28</span></li>
                                            <li><span>29</span></li>
                                            <li><span>30</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-2" />
                    </div>
                </div>
            </section>
            <section id="boxTimeMb" className="box-time box-time-mb active-mb">
                <div className="box-time-mb-content">
                    <div className="time-total">
                        <i className="fa fa-clock-o" aria-hidden="true" />
                        <span id="ten-countdown-mb" />
                    </div>
                    <div className="box-button">
                        <a className="btn btn-cyan">Next Question<i className="fa fa-chevron-circle-right" aria-hidden="true" /></a>
                        <Link to="/part-2" className="btn btn-cyan">Next Part<i className="fa fa-arrow-circle-right" aria-hidden="true" /></Link>
                        <Button className="btn btn-default" onClick={openModal}>Nộp bài</Button>
                    </div>
                </div>
            </section>
            <ResultViewModal isShowModal={isShowModal}
                Closed={closeModal} />
        </div>
    )
}

export default TestingScreen

