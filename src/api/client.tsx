import Axios from "axios"
import variableEx from "utils/VariableEx"


Axios.defaults.baseURL = "https://staging.api.f6.com.vn/edu"
Axios.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded"
Axios.defaults.timeout = 15000
Axios.defaults.timeoutErrorMessage = "Connection timed out!"

const ERROR_COMMON = "Đã xảy ra sự cố, vui lòng thử lại sau."
const ERROR_NETWORKING = "Lỗi mạng: "

export const checkSuccess = (data: any) => {
    if (!variableEx.isEmpty(data) && (!data.error)) {
        const status = data.status ?? data.result
        if (!variableEx.isEmpty(status)) {
            if (typeof status === "boolean") {
                return status
            }

            if (typeof status === "number") {
                return status === 0
            }

            return status === "success"
        }

        return true
    }

    return false
}

class RequestClient {
    async get(endpoint: string, params?: any, headers: Headers = new Headers()) {
        try {
            const response = await Axios.get(endpoint, { params: params, headers: headers })
            // console.debug("RESPONSE: ", response)
            const data = response?.data
            if (!variableEx.isEmpty(data?.error)) {
                return {
                    error: data?.error,
                    errorDes: data?.message ?? data?.errorDes ?? data?.error_description ?? ERROR_COMMON
                }
            }
            return data
        } catch (error) {
            // console.debug("ENDPOINT: ", endpoint)
            return this.handleError(error)
        }
    }

    async post(endpoint: string, body: URLSearchParams | FormData, params?: any, headers: Headers = new Headers()) {
        try {
            const response = await Axios.post(endpoint, body, { params: params, headers: headers })
            // console.debug("RESPONSE: ", response)
            const data = response?.data
            if (!variableEx.isEmpty(data?.error)) {
                return {
                    error: data?.error,
                    errorDes: data?.message ?? data?.errorDes ?? data?.error_description ?? ERROR_COMMON
                }
            }
            return data
        } catch (error) {
            // console.debug("ENDPOINT: ", endpoint)
            return this.handleError(error)
        }
    }

    handleError = (error: any) => {
        // console.debug("RESPONSE ERROR", error)

        const response = error.response
        const data = response?.data
        if (!variableEx.isEmpty(data?.error)) {
            return {
                error: data?.error,
                errorDes: data?.message ?? data?.errorDes ?? data?.error_description ?? ERROR_COMMON
            }
        }

        if (!variableEx.isEmpty(response) && (response.status >= 300)) {
            return {
                error: "netwoking_error",
                errorDes: ERROR_NETWORKING + error.response.status
            }
        }

        return {
            error: "error_common",
            errorDes: error.message ?? ERROR_COMMON
        }
    }
}

const client = new RequestClient()

export default client