module.exports = {
  plugins: [
    [
      "module-resolver",
      {
        root: ["./src"],
        extensions: [".js", ".ts", ".tsx"],
        alias: {
          "api/*": ["api/*"],
          "assets/*": ["assets/*"],
          "constant/*": ["constant/*"],
          "utils/*": ["utils/*"],
          "services/*": ["services/*"],
          "components/*": ["components/*"]
        }
      }
    ]
  ]
};
